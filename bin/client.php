<?php

require __DIR__ . '/../vendor/autoload.php';

use React\EventLoop\Factory;
use React\Socket\Connector;
use React\Socket\ConnectionInterface;
use React\Stream\WritableResourceStream;
use React\Stream\ReadableResourceStream;
use React\Stream\DuplexResourceStream;

$endpoint = 'wss://stream.binance.com:9443';
$stream = '/ws/ETHBTC@aggTrade';

$resource = stream_socket_client($endpoint . $stream, $errno, $errstr);
if (!$resource) {
    echo "ERROR: $errno - $errstr<br />\n";
    exit;
}

$loop = Factory::create();
$stream = new ReadableResourceStream($resource, $loop);

$stream->on('data', function ($chunk) {
    echo $chunk;
});
$stream->on('close', function () {
    echo '[CLOSED]' . PHP_EOL;
});

$stream->write("GET / HTTP/1.1\r\nHost: $endpoint$stream\r\n\r\n");

$loop->run();
